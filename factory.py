#!/usr/bin/python

from twisted.internet import protocol, reactor, ssl
from twisted.protocols import amp
from twisted.python import log

from twisted.web.server import Site
from twisted.web.static import File

from autobahn.twisted.websocket import WebSocketServerFactory
from autobahn.twisted.resource import *

from protocol import AmpServerProtocol, AmpClientProtocol, AmpLogBot, AmpWSocketProtocol
from socks import SocksServer

import hashlib
import time
import sys

class AmpBot():
    
    def __init__(self, host, port, pubIP, priIP, aType, cCert=None, enableIRC=True, ignoreNAT=True):
        self.host = host
        self.port = port
        self.pingRaceDest = 'www.google.com'
        self.ircNet = 'irc.network.net'
        self.iport = None
        self.conTime = None
        self.state = None
        self.ircEnabled = enableIRC
        self.caCert = 'cert/easyrsa/ca.crt'
        self.cliCert = 'cert/easyrsa/ABCli.pem'
        self.servCert = 'cert/easyrsa/ABServ.pem'
        self.contCert = cCert
        self.pubIP = pubIP
        self.priIP = priIP
        self.addrType = aType
        self.isNAT = None
        self.ignoreNAT = ignoreNAT
        self.__sFactory = None
        self.__cFactory = None
        self.__uiFactory = None
        self.__servCon = None
        self.__cliCon = None
        self.__ircCon = None
        self.__uiCon = None
        self.__sServ = None
        
    def getServerFactory(self):
        return self.__sFactory
    
    def getClientFactory(self):
        return self.__cFactory
    
    def getUIFactory(self):
        return self.__uiFactory
    
    def getSocksServer(self):
        return self.__sServ
    
    def hasUI(self):
        if (self.getUIFactory() is not None and self.getUIFactory().getProtocol() is not None):
            return True
        else:
            return False
        
    def initClient(self):
        self.__cFactory = ClientFactory(self)
        self.__cliCon = reactor.connectTCP(self.host, int(self.port), self.__cFactory)
        log.startLogging(sys.stdout)
        
    def initServer(self):
        self.__sFactory = ServerFactory(self)
        self.__servCon = reactor.listenTCP(int(self.port), self.__sFactory)
        log.startLogging(sys.stdout)
        
    def initIrcBot(self):
        m = hashlib.md5()
        hashStr = '%s:W0000000000P!:%d' % (self.host, int(time.time()))
        m.update(hashStr)
        md5 = m.hexdigest()
        self.__ircCon = reactor.connectSSL(self.ircNet, 6697, LogBotFactory(self, '#%s' % (md5)), ssl.ClientContextFactory())
        log.startLogging(sys.stdout)
        
    def initControllerUI(self, port):
        self.__uiFactory = WSocketFactory(self, port)  
        resource = WebSocketResource(self.__uiFactory)
        root = File('ui')
        root.putChild('ws', resource)
        site = Site(root)
        self.__uiCon = reactor.listenTCP(port, site, interface='127.0.0.1')
        log.startLogging(sys.stdout)
        
    def initSocksServer(self):
        self.__sServ = SocksServer('socksPuppet')
        self.__sServ.start()
        
    def getHostPort(self):
        return self.port
    
    def getIntPort(self):
        return self.iport
    
    def getHost(self):
        return self.host
    
    def getConTime(self):
        return self.conTime
    
    def getServerConnection(self):
        return self.__servCon;
    
    def getClientConnection(self):
        return self.__cliCon;
    
    def getIrcConnection(self):
        return self.__ircCon
    
    def getUIConnection(self):
        return self.__uiCon
        
    def start(self):
        self.conTime = int(time.time())
        reactor.run()
        
    def stop(self):
        reactor.stop()

class ServerFactory(protocol.ServerFactory):
    protocol = AmpServerProtocol
        
    def __init__(self, i):
        self.__inst = i
        self.__prots = []
        self.__peers = []
        self.__conts = []
        self.cbRes = []
        
    def getInstance(self):
        return self.__inst
            
    def getPeers(self):
        return self.__peers
        
    def getProts(self):
        return self.__prots
    
    def getControllers(self):
        return self.__conts
    
    def getControllerProts(self):
        cProts = []
        cnts = self.getControllers()
        prs = self.getProts()
        for p in prs:
            for c in cnts:
                cHost = c[0]
                pPeer = p.transport.getPeer()
                if (cHost.host == pPeer.host and cHost.port == pPeer.port):
                    cProts.append(p)
        return cProts
    
    def getSelectedProt(self, selPeer):
        sProt = None
        cProts = self.getControllerProts()
        prs = self.getProts()
        for p in prs:
            if (p not in cProts):
                selSplit = selPeer.split(':')
                pPeer = p.transport.getPeer()
                if (str(selSplit[0]) == str(pPeer.host) and int(selSplit[1]) == int(pPeer.port)):
                    sProt = p
        return sProt
            
    def buildProtocol(self, addr):
        prot = protocol.ServerFactory.buildProtocol(self, addr)
        self.__prots.append(prot)
        return prot
        
    def startFactory(self):
        protocol.ServerFactory.startFactory(self)
        self.__conts = []
        self.__peers = []
        self.__prots = []
        if ((self.getInstance().state == None or self.getInstance().state == 'SRV') and self.getInstance().ircEnabled):
            self.getInstance().initIrcBot()
        
    def stopFactory(self):
        protocol.ServerFactory.stopFactory(self)
        if (self.getInstance().getIrcConnection() is not None):
            self.getInstance().getIrcConnection().disconnect()
        if (len(self.__prots) > 0):
            for p in self.__prots:
                log.msg('Shutting down and stopping threads on protocol %s..' % (p))
                p.stopBeacons()
                p.transport.loseConnection()
                
class ClientFactory(protocol.ClientFactory):
    protocol = AmpClientProtocol
        
    def __init__(self, i):
        self.__inst = i
        self.__peers = []
        self.__prot = None
        
    def getInstance(self):
        return self.__inst
    
    def getProtocol(self):
        return self.__prot
            
    def getPeers(self):
        return self.__peers
        
    def setPeers(self, peers):
        self.__peers = peers
        
    def buildProtocol(self, addr):
        self.__prot = protocol.ClientFactory.buildProtocol(self, addr)
        return self.__prot
        
    def startFactory(self):
        protocol.ClientFactory.startFactory(self)
        
    def stopFactory(self):
        protocol.ClientFactory.stopFactory(self)

class LogBotFactory(protocol.ClientFactory):

    def __init__(self, i, channel):
        self.__inst = i
        self.channel = channel
        
    def getInstance(self):
        return self.__inst

    def buildProtocol(self, addr):
        p = AmpLogBot()
        p.factory = self
        return p

    def clientConnectionLost(self, connector, reason):
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print('IRC connection failed: %s' % (reason))
        
class WSocketFactory(WebSocketServerFactory):
    protocol = AmpWSocketProtocol
        
    def __init__(self, i, port):
        self.__inst = i
        self.url = "ws://localhost:%s" % (port)
        WebSocketServerFactory.__init__(self)
        self.__prot = None
        
    def getInstance(self):
        return self.__inst
    
    def getProtocol(self):
        return self.__prot
    
    def buildProtocol(self, addr):
        self.__prot = WebSocketServerFactory.buildProtocol(self, addr)
        return self.__prot
        
    def startFactory(self):
        protocol.ServerFactory.startFactory(self)
        
    def stopFactory(self):
        protocol.ServerFactory.stopFactory(self)
