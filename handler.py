#!/usr/bin/python 

from twisted.protocols import amp
from twisted.protocols.amp import Integer, String, Float, Boolean, TooLong
from twisted.python import log
from twisted.python.failure import Failure
from twisted.internet import reactor
from twisted.internet.task import deferLater

from util import isUp, isNATPeer

import time, json, base64

class CallbackHandler():
    
    def __init__(self, factory, protocol, data, peer):
        self.__factory = factory
        self.__prot = protocol
        self.cbData = data
        self.cbPeer = peer
        
    def getFactory(self):
        return self.__factory
    
    def getProt(self):
        return self.__prot
        
    def handle(self):
        if ('pingtime' in self.cbData.keys()):
            # Handle ping race
            self.handlePingRace()
        if ('newserver' in self.cbData.keys()):
            # Handle server switch
            self.handleNewServer()
        if ('sup' in self.cbData.keys()):
            # Handle sup response 
            self.handleSupResponse()
            
    def handlePingRace(self):
        # Add results and assoc peers to list until we can come to a decision
        res = self.getFactory().cbRes
        res.append((self.cbData['pingtime'], self.cbPeer))
        # Leave below just in case NAT trav ever becomes feasible..
        #numRes = (len(self.getFactory().getPeers()))
        numRes = 0
        for p in self.getFactory().getPeers():
            if (self.getFactory().getInstance().ignoreNAT is True or isNATPeer(self.getFactory().getPeers(), p[0]) is False):
                numRes += 1
        if (len(res) == numRes):
            from protocol import NewServerCommand
            log.msg('Got ping results from all peers..')
            sortedRes = sorted(res, key=lambda time: time[0])
            log.msg('Sorted results: %s' % (sortedRes))
            newRes = sortedRes[0]
            newServ = newRes[1]
            host = newServ.host
            # Check NAT status of chosen server and translate to private address if necessary 
            for p in self.getFactory().getPeers():
                pr = p[0]
                if (pr.host == host and pr.port == newServ.port and p[-1] is True):
                    host = p[-2]
                    print('Translating host to private NAT address: %s' % (host))
            newServStr = '%s:%d' % (host, newServ.port)
            log.msg('New server will be: %s, informing peers..' % (newServ))
            for pr in self.getFactory().getProts():
                peer = pr.transport.getPeer()
                d = pr.callRemote(NewServerCommand, u=int(time.time()), newserver=newServStr)
                d.addCallbacks(pr.printData, pr.printError, callbackKeywords={'sucPeer': peer}, errbackKeywords={'errPeer': peer})
            # Reset state to CLI and stop factory
            self.getFactory().getInstance().host = newServ.host
            # ALWAYS clear callback results after collection!
            self.getFactory().cbRes = []
            time.sleep(5)
            
    def handleNewServer(self):
        self.getProt().switchServer()
        
    def handleSupResponse(self):
        # Set NAT status
        self.getFactory().getInstance().isNAT = self.cbData['sup']
        if (self.getFactory().getInstance().state != 'CNT'):
            log.msg('Setting state to None..')
            self.getFactory().getInstance().state = None
        else:
            if (isUp('127.0.0.1', 8080) is False):
                log.msg('Running in controller mode, launching interface on localhost:8080..')
                self.getFactory().getInstance().initControllerUI(8080)
            else:
                log.msg('Running in controller mode, interface on localhost:8080..')
                
class CommandResultHandler():
    
    def __init__(self, factory, protocol, data, peer):
        self.__factory = factory
        self.__prot = protocol
        self.cbData = data
        self.cbPeer = peer
        
    def getFactory(self):
        return self.__factory
    
    def getProt(self):
        return self.__prot
    
    def handle(self, command):
        if ('peer' in self.cbData.keys() and 'output' in self.cbData.keys() and 'exit' in self.cbData.keys()):
            # Handle command exec
            self.handleCmdExec(command)
                
    def handleCmdExec(self, command):
        res = self.getFactory().cbRes
        res.append((self.cbData['exit'], self.cbData['output'], self.cbPeer))
        cProts = self.getFactory().getControllerProts()
        from protocol import CommandResult
        if (self.cbData['peer'] != 'All'):
            # Send result immediately
            resCount = 1
        else:
            resCount = len(self.getFactory().getPeers())
        if (len(res) == resCount):
            log.msg('Got exec results from all peers..')
            results = []
            for r in res:
                pStr = '%s:%s' % (r[2].host, r[2].port)
                resData = {'exit': r[0], 'output': r[1], 'peer': pStr}
                results.append(resData)
            # ALWAYS clear callback results after collection!
            self.getFactory().cbRes = []
            try:
                # Convert to b64
                resB64 = base64.b64encode(json.dumps(results))
                # Chop up the message into 64k segments
                chunkSize = 65535
                chunks = [resB64[i:i + chunkSize] for i in range(0, len(resB64), chunkSize)]
                for i in range(0, len(chunks)):
                    chunk = chunks[i]
                    for c in cProts:
                        peer = c.transport.getPeer()
                        d = c.callRemote(CommandResult, u=int(time.time()), result=str(chunk), encoding='b64', chunk='%s/%s' % ((i + 1), len(chunks)), cmd=str(command))
                        d.addCallbacks(c.printData, c.printError, callbackKeywords={'sucPeer': peer}, errbackKeywords={'errPeer': peer})
            except Exception as e:
                log.msg('Exception while sending command result: %s' % (e))
        