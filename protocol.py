#!/usr/bin/python

from twisted.words.protocols import irc
from twisted.internet.address import IPv4Address
from twisted.internet.protocol import Protocol, ClientFactory
from twisted.internet.task import LoopingCall
from twisted.internet import protocol, reactor, ssl
from twisted.protocols import amp
from twisted.protocols.amp import CommandLocator, UnhandledCommand, Integer, String, Float, Boolean, TooLong
from twisted.python import log

from autobahn.twisted.websocket import WebSocketServerProtocol

from sys import stdout, exit
import time, json, base64
import hashlib

from handler import CallbackHandler, CommandResultHandler
from util import Exec, isUp, isControllerPeer, isNATPeer


class SupCommand(amp.Command):
    arguments = [('u', amp.Integer()), ('msg', amp.String()), ('pubIp', amp.String()), ('priIp', amp.String())]
    response = [('sup', amp.Boolean())]
    
class SendPeersCommand(amp.Command):
    arguments = [('u', amp.Integer()), ('peers', amp.ListOf(amp.String()))]
    response = [('peers', amp.Boolean())]
    
class PingCommand(amp.Command):
    arguments = [('u', amp.Integer()), ('target', amp.String())]
    response = [('pingtime', amp.Float())]
    
class NewServerCommand(amp.Command):
    arguments = [('u', amp.Integer()), ('newserver', amp.String())]
    response = [('newserver', amp.Boolean())]
    
class ExecCommand(amp.Command):
    arguments = [('u', amp.Integer()), ('cmd', amp.String()), ('peer', amp.String()), ('cmdReq', amp.String())]
    response = [('exec_command', amp.Boolean())]
    
class CommandRequest(amp.Command):
    arguments = [('u', amp.Integer()), ('cmd', amp.String()), ('data', amp.String())]
    response = [('request', amp.Boolean())]
    
class CommandResult(amp.Command):
    arguments = [('u', amp.Integer()), ('result', amp.String()), ('encoding', amp.String()), ('chunk', amp.String()), ('cmd', amp.String())]
    response = [('result', amp.Boolean())]

class AmpServerProtocol(amp.AMP):
    
    def __init__(self):
        self.__certParams = None
        self.__prt = LoopingCall(self.sendPeers)
        self.chunkStore = []
        
    def locateResponder(self, name):
        #log.msg('Server locateResponder called for command: %s. TLS state: %s' % (name, self.transport.TLS))
        if (name == 'StartTLS' or self.transport.TLS is True):
            return CommandLocator.locateResponder(self, name)
        else:
            log.msg('Invalid TLS context, dropping connection!')
            self.transport.loseConnection()
            return None
        
    def startBeacons(self):
        self.__prt.start(20.0)
        if (self.factory.getInstance().state == 'INT'):
            reactor.callLater(30, self.pingRace)
        
    def stopBeacons(self):
        if (self.__prt.running):
            self.__prt.stop()
                
    def sendPeers(self):
        numPeers = len(self.factory.getPeers())
        if (self.transport is not None):
            peer = self.transport.getPeer()
            peersStr = []
            hPort = self.factory.getInstance().getHostPort()
            log.msg('Sending peerlist.. (%d peers connected)' % (numPeers))
            for p in self.factory.getPeers():
                pr = p[0]
                etime = p[1]
                uname = p[2]
                pub = p[3]
                pri = p[4]
                isNAT = p[5]
                pStr = '%s|%s|%s|%s|%s|%s|%s|%s' % (pr.host, hPort, etime, pr.port, uname, pub, pri, isNAT)
                peersStr.append(pStr)
            try:
                d = self.callRemote(SendPeersCommand, u=int(time.time()), peers=peersStr)
                d.addCallbacks(self.printData, self.printError, callbackKeywords={'sucPeer': peer}, errbackKeywords={'errPeer': peer})
            except Exception as e:
                log.msg('Error sending peer list to peer: %s (%s)' % (peer, e))
            
    def pingRace(self, toAll=False):
        if (toAll is False):
            if (self.transport is None):
                return None
            peer = self.transport.getPeer()
            if (isControllerPeer(self.factory.getControllers(), peer) is False and (self.factory.getInstance().ignoreNAT is True or isNATPeer(self.factory.getPeers(), peer) is False)):
                log.msg('Acting as intermediary, starting ping race to: %s' % (self.factory.getInstance().pingRaceDest))
                try:
                    d = self.callRemote(PingCommand, u=int(time.time()), target=str(self.factory.getInstance().pingRaceDest))
                    d.addCallbacks(self.printData, self.printError, callbackKeywords={'sucPeer': peer}, errbackKeywords={'errPeer': peer})
                except Exception as e:
                    log.msg('Error sending ping command to peer: %s (%s)' % (peer, e))    
        else:
            # Send to all known peers
            prots = self.factory.getProts()
            for pr in prots:
                peer = pr.transport.getPeer()
                if (isControllerPeer(self.factory.getControllers(), peer) is False and (self.factory.getInstance().ignoreNAT is True or isNATPeer(self.factory.getPeers(), peer) is False)):
                    log.msg('Acting as intermediary, starting ping race to: %s' % (self.factory.getInstance().pingRaceDest))
                    try:
                        d = pr.callRemote(PingCommand, u=int(time.time()), target=str(self.factory.getInstance().pingRaceDest))
                        d.addCallbacks(pr.printData, pr.printError, callbackKeywords={'sucPeer': peer}, errbackKeywords={'errPeer': peer})
                    except Exception as e:
                        log.msg('Error sending ping command to peer: %s (%s)' % (peer, e))
            
    def printData(self, data, *args, **kwargs):
        log.msg('Deferred result: %s, from peer: %s' % (data, kwargs['sucPeer']))
        cbh = CallbackHandler(self.factory, self, data, kwargs['sucPeer'])
        cbh.handle()
        
    def printError(self, data, *args, **kwargs):
        log.msg('Deferred error: %s, from peer: %s' % (data, kwargs['errPeer']))
        
    @CommandRequest.responder
    def cmdRequest(self, u, cmd, data):
        try:
            log.msg('Got command request!! %s' % (data))
            jData = json.loads(data)
            if (cmd == 'exec' or cmd == 'getfile'):
                pr = jData['peer']
                if (cmd == 'exec'):
                    command = jData['cmd']
                if (cmd == 'getfile'):
                    path = jData['path']
                    command = 'cat %s | base64' % (path)
                for p in self.factory.getProts():
                    if (p not in self.factory.getControllerProts()):
                        pPeer = p.transport.getPeer()
                        if (pr != 'All' and ':' in pr):
                            selProt = self.factory.getSelectedProt(pr)
                            if (p is not selProt):
                                # Skip unmatching peers
                                log.msg('Skipping peer: %s' % (pPeer))
                                continue
                        d = p.callRemote(ExecCommand, u=int(time.time()), cmd=str(command), peer=str(pr), cmdReq=str(cmd))
                        d.addCallbacks(p.printData, p.printError, callbackKeywords={'sucPeer': pPeer}, errbackKeywords={'errPeer': pPeer})
            if (cmd == 'newserver'):
                if (jData['ping-dest'] != 'default'):
                    self.factory.getInstance().pingRaceDest = jData['ping-dest']
                self.pingRace(True)
        except Exception as e:
            log.msg('Exception sending Command: %s' % (e))
            return {'request': False}
        return {'request': True}
    
    @CommandResult.responder
    def cmdResult(self, u, result, encoding, chunk, cmd):
        log.msg('Got command result! Chunk: %s, command: %s, encoding: %s' % (chunk, cmd, encoding))
        # Add chunks to chunkStore
        curChunk = chunk.split('/')[0]
        chunkRangeEnd = chunk.split('/')[1]
        self.chunkStore.append(result)
        if (curChunk == chunkRangeEnd):
            resStr = ''
            for ch in self.chunkStore:
                resStr += ch
            if (encoding == 'b64'):
                resStr = base64.b64decode(resStr)
            # ALWAYS Clear the chunk store after collection!
            self.chunkStore = []
            # Handle the full command result
            crh = CommandResultHandler(self.factory, self, json.loads(resStr), self.transport.getPeer())
            crh.handle(cmd)
        return {'result': True} 

    @SupCommand.responder
    def sup(self, u, msg, pubIp, priIp):
        peer = self.transport.getPeer()
        pCert = self.peerCertificate
        isNAT = False
        if (peer.host not in [pubIp, priIp]):
            isNAT = True
        if (pCert.getSubject()['commonName'] == 'ABCli'):
            # Add peer and epoch time of connection if normal client
            self.factory.getPeers().append((peer, u, msg, pubIp, priIp, isNAT))
        if (pCert.getSubject()['commonName'] == 'ABCont'):
            log.msg('Controller connected at %s:' % (peer))
            # Add controller client
            self.factory.getControllers().append((peer, u, msg, pubIp, priIp, isNAT))
        log.msg('Got sup! Starting beacons.. Msg: %s, time: %d. From peer: %s' % (msg, u, peer))
        self.startBeacons()
        return {'sup': isNAT}

    @amp.StartTLS.responder
    def startTLS(self):
        log.msg('Server startTLS responder returning cert params..')
        return self.__certParams
    
    def switchServer(self):
        # TODO: Save current server and peerlist to temp file in case of shutdown
        self.factory.getInstance().state = 'CLI'
        self.factory.getInstance().getServerConnection().stopListening()
        self.transport.loseConnection()
        time.sleep(5)
    
    def initCert(self):
        servCert = self.factory.getInstance().servCert
        cert = ssl.PrivateCertificate.loadPEM(open(servCert).read())
        cacert = ssl.Certificate.loadPEM(open(self.factory.getInstance().caCert).read())
        self.__certParams = {'tls_localCertificate' : cert, 'tls_verifyAuthorities' : [cacert]}
    
    def connectionMade(self):
        amp.AMP.connectionMade(self)
        self.initCert()
        
    def connectionLost(self, reason):
        peer = self.transport.getPeer()
        amp.AMP.connectionLost(self, reason)
        # Remove peer and corresponding prot from peer list
        self.stopBeacons()
        for p in self.factory.getPeers():
            if (p[0] == peer):
                log.msg('Removing peer due to lost connection: %s' % (peer))
                self.factory.getPeers().remove(p)
        for pr in self.factory.getProts():
            if (pr == self):
                log.msg('Removing protocol due to lost connection: %s' % (peer))
                self.factory.getProts().remove(pr)
        for c in self.factory.getControllers():
            if (c[0] == peer):
                log.msg('Removing controller peer due to lost connection: %s' % (peer))
                self.factory.getControllers().remove(c)
        if (self.factory.getInstance().state == 'CLI'):
            self.factory.getInstance().state = None
            self.factory.getInstance().initClient()
     
class AmpClientProtocol(amp.AMP):
    
    def __init__(self): 
        self.__certParams = None
        self.__cliExec = Exec()
        self.chunkStore = []
        
    def locateResponder(self, name):
        #log.msg('Client locateResponder called for command: %s. TLS state: %s' % (name, self.transport.TLS))
        if (self.transport.TLS is True):
            return CommandLocator.locateResponder(self, name)
        else:
            log.msg('Invalid TLS context, dropping connection!')
            self.transport.loseConnection()
            return None
    
    @SendPeersCommand.responder
    def sendPeers(self, u, peers):
        log.msg('Got peers from server: %s, time: %d' % (peers, u))
        newPeerList = []
        for p in peers:
            pSplit = p.split('|')
            newPeerList.append((pSplit[0], pSplit[1], pSplit[2], pSplit[3], pSplit[4], pSplit[5], pSplit[6], pSplit[7]))
        self.factory.setPeers(newPeerList)
        if (self.factory.getInstance().hasUI()):
            self.factory.getInstance().getUIFactory().getProtocol().sendPeers(newPeerList)
        return {'peers': True}
    
    @PingCommand.responder
    def ping(self, u, target):
        log.msg('Got ping command from server, starting race to: %s. Time: %d' % (target, u))
        cmd = self.__cliExec.execCmd("ping -c 1 %s | grep time= | awk '{ print $(NF-1) }'" % (target))
        timeStr = self.__cliExec.getOutputLines(cmd)
        if (len(timeStr) > 0):
            timeStr = timeStr[0]
            timeStrSplit = timeStr.split('=')
            return {'pingtime': float(timeStrSplit[1])}
        else:
            return {'pingtime': 999.9}
    
    @NewServerCommand.responder
    def newServer(self, u, newserver):
        # TODO: Save current server and peerlist to temp file in case of shutdown
        log.msg('New server to connect to: %s. Time: %d' % (newserver, u))
        nServSplit = newserver.split(':')
        i = self.factory.getInstance()
        addrList = [i.pubIP]
        for ip in i.priIP:
            if ('127.0.' not in ip):
                addrList.append(ip)
        if (nServSplit[0] in addrList and i.getIntPort() == int(nServSplit[1])):
            log.msg('Im the new server! Waiting for intermediary to stop..')
            i.state = 'SRV'
        else:
            log.msg('Waiting for intermediary to stop..')
            i.host = nServSplit[0]
            i.state = 'CLI'
            time.sleep(5)
        
        if (self.factory.getInstance().hasUI()):
            self.factory.getInstance().getUIFactory().getProtocol().finishServerSwitch(newserver)
        return {'newserver': True}
    
    @ExecCommand.responder
    def execCommand(self, u, cmd, peer, cmdReq):
        try:
            proc = self.__cliExec.execCmd(cmd)
            result = self.__cliExec.getOutputLines(proc)
            exit = self.__cliExec.getExitCode(proc)
            if (len(result) > 0):
                result = result[0]
            else:
                result = str(result)
            resData = {'exit': exit, 'output': result, 'peer': peer}
            # Convert to b64
            resB64 = base64.b64encode(json.dumps(resData))
            # Chop up the message into 64k segments
            chunkSize = 65535
            chunks = [resB64[i:i + chunkSize] for i in range(0, len(resB64), chunkSize)]
            for i in range(0, len(chunks)):
                chunk = chunks[i]
                peer = self.transport.getPeer()
                d = self.callRemote(CommandResult, u=int(time.time()), result=str(chunk), encoding='b64', chunk='%s/%s' % ((i + 1), len(chunks)), cmd=str(cmdReq))
                d.addCallbacks(self.printData, self.printError, callbackKeywords={'sucPeer': peer}, errbackKeywords={'errPeer': peer})
        except Exception as e:
            log.msg('Exception: %s' % (e))
        return {'exec_command': True}
    
    @CommandResult.responder
    def cmdResult(self, u, result, encoding, chunk, cmd):
        log.msg('Got command result! Chunk: %s, command: %s, encoding: %s' % (chunk, cmd, encoding))
        # Add chunks to chunkStore
        curChunk = chunk.split('/')[0]
        chunkRangeEnd = chunk.split('/')[1]
        self.chunkStore.append(result)
        if (curChunk == chunkRangeEnd):
            resStr = ''
            for ch in self.chunkStore:
                resStr += ch
            if (encoding == 'b64'):
                resStr = base64.b64decode(resStr)
            # ALWAYS Clear the chunk store after collection!
            self.chunkStore = []
            # Save file contents to disk for each peer
            if (cmd == 'getfile'):
                ps = json.loads(resStr)
                for p in ps:
                    if (p['exit'] != 0):
                        p['output'] = 'Non zero exit code for command, no file saved'
                    else:
                        # Decode again..
                        try:
                            fCont = base64.b64decode(p['output'])
                            m = hashlib.md5()
                            m.update(p['peer'])
                            md5FileName = '%s_%s' % (m.hexdigest(), p['peer'])
                            f = open('/tmp/%s' % (md5FileName), 'w')
                            f.write(fCont)
                            f.close()
                            p['output'] = 'File saved to: /tmp/%s' % (md5FileName)
                        except Exception as e:
                            p['output'] = 'Exception while decoding and writing file: %s' % (e)
                resStr = json.dumps(ps)
            # Forward the result to the UI if available and all chunks received
            if (self.factory.getInstance().hasUI()):
                self.factory.getInstance().getUIFactory().getProtocol().sendCommandResult(resStr, cmd)
        return {'result': True} 
    
    def printData(self, data, *args, **kwargs):
        log.msg('Deferred result: %s, from peer: %s' % (data, kwargs['sucPeer']))
        cbh = CallbackHandler(self.factory, self, data, kwargs['sucPeer'])
        cbh.handle()
        
    def printError(self, data, *args, **kwargs):
        log.msg('Deferred error: %s, from peer: %s' % (data, kwargs['errPeer']))
        r = data.trap(UnhandledCommand)
        if (r == UnhandledCommand):
            log.msg('Command was unhandled on the remote side, usually due to invalid TLS context. Dropping connection')
            self.transport.loseConnection()
            self.factory.getInstance().stop()
            
    def initCert(self):
        if (self.factory.getInstance().contCert is None):
            cliCert = self.factory.getInstance().cliCert
        else:
            cliCert = self.factory.getInstance().contCert
        cert = ssl.PrivateCertificate.loadPEM(open(cliCert).read())
        cacert = ssl.Certificate.loadPEM(open(self.factory.getInstance().caCert).read())
        self.__certParams = {'tls_localCertificate' : cert, 'tls_verifyAuthorities' : [cacert]}
        
    def sendCommandRequest(self, command, request):
        try:
            peer = self.transport.getPeer()
            d = self.callRemote(CommandRequest, u=int(time.time()), cmd=command, data=json.dumps(request))
            d.addCallbacks(self.printData, self.printError, callbackKeywords={'sucPeer': peer}, errbackKeywords={'errPeer': peer})
        except Exception as e:
            log.msg('Exception sending CommandRequest: %s' % (e))
            
    def getIntermediarys(self, peers):
        noNAT = []
        for p in peers:
            if (self.factory.getInstance().ignoreNAT is False):
                if (p[7] != 'True'):
                    noNAT.append(p)
            else:
                noNAT.append(p)
        return noNAT
    
    def connectionMade(self):
        amp.AMP.connectionMade(self)
        self.initCert()
        log.msg('Calling startTLS..')
        self.callRemote(amp.StartTLS, **self.__certParams)
        peer = self.transport.getPeer()
        pCert = self.__certParams['tls_localCertificate']
        cmd = self.__cliExec.execCmd('uname -a')
        uname = self.__cliExec.getOutputLines(cmd)
        uname = uname[0]
        if (pCert.getSubject()['commonName'] == 'ABCont'):
            self.factory.getInstance().state = 'CNT'
        priIP = '0.0.0.0'
        for ip in self.factory.getInstance().priIP:
            if ('127.0.' not in ip):
                priIP = ip
        d = self.callRemote(SupCommand, u=int(time.time()), msg=uname, pubIp=str(self.factory.getInstance().pubIP), priIp=str(priIP))
        d.addCallbacks(self.printData, self.printError, callbackKeywords={'sucPeer': peer}, errbackKeywords={'errPeer': peer})
        # Set the connection time and internal port
        host = self.transport.getHost()
        self.factory.getInstance().conTime = str(int(time.time()))
        self.factory.getInstance().iport = host.port
        
    def connectionLost(self, reason):
        if (self.transport is not None and self.transport.TLS is False):
            return None
        amp.AMP.connectionLost(self, reason)
        #log.msg('Connection lost due to: %s' % (reason))
        i = self.factory.getInstance()
        c = 1
        if (i.state == 'CNT'):
            if (i.hasUI()):
                i.getUIFactory().getProtocol().startServerSwitch()
            i.state = None
        if (i.state == 'CLI'):
            log.msg('Waiting for server to start listening..')
            while (isUp(i.host, i.port) is False):
                time.sleep(1)
                log.msg('%d..' % (c))
                c += 1
                if (c >= 30):
                    log.msg('Giving up!')
                    break;
            log.msg('Connection lost, state is CLI')
            i.initClient()
        if (i.state == 'SRV' or i.state == None):
            log.msg('Waiting for server to stop listening..')
            while (isUp(i.host, i.port)):
                time.sleep(1)
                log.msg('%d..' % (c))
                c += 1
                if (c >= 30):
                    log.msg('Giving up!')
                    break;
        if (i.state == 'SRV'):
            log.msg('Connection lost, state is SRV')
            i.initServer()
        if (isUp(i.host, i.getHostPort()) is False and i.state == None):
            peers = self.factory.getPeers()
            if (len(peers) > 0):
                log.msg('Server died, switching to intermediary..')
                nomPeers = self.getIntermediarys(peers)
                if (len(nomPeers) > 0):
                    nomPeer = nomPeers[-1]
                    log.msg('Peer nominated for intermediary. Host: %s, port: %s, utime: %s, iport: %s' % (nomPeer[0], nomPeer[1], nomPeer[2], nomPeer[3]))
                    if (i.getHost() == nomPeer[0] and i.getConTime() == nomPeer[2] and i.getIntPort() == int(nomPeer[3])):
                        log.msg('Im the intermediary! Stopping client and starting intermediary server in 5 seconds..')
                        i.state = 'INT'
                        time.sleep(5)
                        i.initServer()
                    else:
                        log.msg('Waiting 15 seconds to join intermediary server..')
                        i.host = nomPeer[0]
                        time.sleep(15)
                        i.initClient()
                else:
                    log.msg('Couldnt find a suitable intermediary! All peers are NATed')
        else:
            if (i.state == None):
                print('Attempting reconnection to server')
                i.initClient()
            
class AmpLogBot(irc.IRCClient):
    def __init__(self):
        self.masternick = "master"
        self.botnick = "bot"
        self.password = "helloampbot123"
    
    def connectionMade(self):
        irc.IRCClient.connectionMade(self)
        log.msg("[connected at %s]" %  time.asctime(time.localtime(time.time())))

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)
        log.msg("[disconnected at %s]" %  time.asctime(time.localtime(time.time())))

    # callbacks for events

    def signedOn(self):
        self.join(self.factory.channel)
        
    def lineReceived(self, line):
        irc.IRCClient.lineReceived(self, line)
        log.msg('Received line: %s' % (line))

    def joined(self, channel):
        log.msg("[I have joined %s]" % channel)
        log.msg("Making room private and setting topic..")
        self.sendLine("cs register %s %s" % (channel, self.password))
        self.sendLine("cs set %s mlock +inst" % (channel))
        self.sendLine("cs set %s restricted on" % (channel))
        self.sendLine("cs access %s add %s" % (channel, self.masternick))
        self.topic(channel, "ABot")
        self.invite(self.masternick, channel)

    def privmsg(self, user, channel, msg):
        user = user.split('!', 1)[0]
        log.msg("<%s> %s" % (user, msg))
        
        # Check to see if they're sending me a private message
        if channel == self.botnick:
            msg = "It isn't nice to whisper!  Play nice with the group."
            self.msg(user, msg)
            return

        # Otherwise check to see if it is a message directed at me
        if msg.startswith(self.botnick + ":"):
            msg = "%s: I am a log bot" % user
            self.msg(channel, msg)
            log.msg("<%s> %s" % (self.botnick, msg))

    def action(self, user, channel, msg):
        user = user.split('!', 1)[0]
        log.msg("* %s %s" % (user, msg))

    # irc callbacks

    def irc_NICK(self, prefix, params):
        old_nick = prefix.split('!')[0]
        new_nick = params[0]
        log.msg("%s is now known as %s" % (old_nick, new_nick))

    def alterCollidedNick(self, nickname):
        return nickname + '^^'
    
class AmpWSocketProtocol(WebSocketServerProtocol):
    
    def onConnect(self, request):
        log.msg("WebSocket connection request: %s" % (request))
        
    def onOpen(self):
        WebSocketServerProtocol.onOpen(self)
        self.sendCurrentServer()
        
    def onClose(self, wasClean, code, reason):
        WebSocketServerProtocol.onClose(self, wasClean, code, reason)
        
    def startServerSwitch(self):
        data = {'action': 'server-switch-start', 'u': int(time.time()), 'data': ''}
        jData = json.dumps(data)
        self.sendMessage(jData, False)
        
    def finishServerSwitch(self, newserv):
        data = {'action': 'server-switch-finish', 'u': int(time.time()), 'data': newserv}
        jData = json.dumps(data)
        self.sendMessage(jData, False)
        
    def sendCurrentServer(self):
        data = {'action': 'set-server', 'u': int(time.time()), 'data': '%s:%s' % (self.factory.getInstance().host, self.factory.getInstance().iport)}
        jData = json.dumps(data)
        self.sendMessage(jData, False)
        
    def sendPeers(self, peers):
        data = {'action': 'set-peers', 'u': int(time.time()), 'data': peers}
        jData = json.dumps(data)
        self.sendMessage(jData, False)
        
    def sendCommandResult(self, result, cmd):
        data = {'action': 'cmd-result', 'u': int(time.time()), 'data': result}
        jData = json.dumps(data)
        self.sendMessage(jData, False)
    
    def connectionMade(self):
        WebSocketServerProtocol.connectionMade(self)
        
    def connectionLost(self, reason):
        WebSocketServerProtocol.connectionLost(self, reason)
    
    def onMessage(self, payload, isBinary):
        if (isBinary is False):
            jData = json.loads(payload.decode('utf8'))
            log.msg('Recieved message from browser: %s' % (jData))
            action = jData['action']
            if (action == 'get-server'):
                self.sendCurrentServer()
            if (action == 'cmd-exec'):
                self.factory.getInstance().getClientFactory().getProtocol().sendCommandRequest('exec', jData)
            if (action == 'new-serv'):
                self.factory.getInstance().getClientFactory().getProtocol().sendCommandRequest('newserver', jData)
            if (action == 'get-file'):
                self.factory.getInstance().getClientFactory().getProtocol().sendCommandRequest('getfile', jData)
        