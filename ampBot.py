#!/usr/bin/python

from util import isUp, getPublicIP, getPrivateIPs, isDHCPAnalytic
from factory import AmpBot

import sys
import os.path

initHost = '127.0.0.1'
initPort = 50105
contCertPath = None

if (len(sys.argv) > 1):
    if (os.path.isfile(sys.argv[1]) is False):
        print('Controller cert %s does not exist' % (sys.argv[1]))
        sys.exit(1)
    else:
        contCertPath = sys.argv[1]
        initHost = raw_input('Enter the current servers IP: ')
        initPort = raw_input('Enter the current servers port: ')
  
pubIp = getPublicIP()
priIps = getPrivateIPs(False)
if (isDHCPAnalytic(pubIp, priIps)):
    aType = 'dyn'
else:
    aType = 'stat'  

a = AmpBot(initHost, initPort, pubIp, priIps, aType, contCertPath, False)

if (contCertPath is None):
    print('Starting AmpBot..')
else:
    print('Starting AmpBot in controller mode.. (using cert: %s)' % (contCertPath))
print('Initial host: %s:%s' % (initHost, initPort))
print('Public IP: %s' % (pubIp))
print('Private IP(s): %s' % (priIps))
print('Address type: %s' % (aType))

print('Attempting connection to initial host: %s:%s' % (initHost, initPort))
if ((initHost == pubIp or initHost in priIps) and isUp(initHost, initPort) is False):
    print('Connection failed or initHost match, becoming server')
    a.initServer()
    a.start()
else:
    print('Connection success, becoming client')
    a.initClient()
    a.start()
