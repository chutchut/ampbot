#!/usr/bin/python

import sys
from util import Exec

class SocksServer():
    
    def __init__(self, pw, sPort=8388, timeout=600):
        self.password = pw
        self.servPort = sPort
        self.timeout = 600
        self.__method = 'aes-256-cfb'
        self.__execCli = Exec()
        self.__pid = None
        
    def isRunning(self):
        p = self.__execCli.execCmd("ps -ef | grep ssserver | grep -v grep | awk '{ print $2 }'")
        pOut = self.__execCli.getOutputLines(p)
        if (len(pOut) > 0):
            return pOut[0].split('\n')
        else:
            return False
        
    def start(self):
        # Check if running already
        r = self.isRunning()
        if (r is not False):
            print('Already running, stopping now..')
            for p in r:
                self.stop(p)
        # Start in bg with & and grab PID
        p = self.__execCli.execCmd('nohup $(which ssserver) -p %s -k %s -m %s -t %s &> /dev/null &' % (self.servPort, self.password, self.__method, self.timeout), sout=None, serr=None)
        r = self.isRunning()
        if (r is not False):
            pid = r[0]
            print('SOCKS server started..')
            print('PID of ssserver: %s' % (pid))
            self.__pid = pid
        else:
            print('Couldnt get the PID for the SOCKS server!')
            
    def stop(self, pid=None):
        if (pid is None):
            pid = self.__pid
        print('Attempting kill with -15 (%s)' % (pid))
        p = self.__execCli.execCmd('kill -15 %s' % (pid))
        ex = self.__execCli.getExitCode(p)
        if (ex != 0):
            print('Attempting kill with -9 (%s)' % (pid))
            p = self.__execCli.execCmd('kill -9 %s' % (pid))
            ex = self.__execCli.getExitCode(p)
        