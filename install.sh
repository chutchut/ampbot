#!/bin/bash

who=$(whoami)
if [[ "$who" != "root" ]]; then
	echo "Run as root"
	exit 1
fi

echo "Checking for pip.."
if [[ $(which pip &> /dev/null; echo $?) != 0 ]]; then
	echo "pip is not installed! Installing now from https://bootstrap.pypa.io/get-pip.py.."
	wget https://bootstrap.pypa.io/get-pip.py
	chmod +x get-pip.py
	python2 get-pip.py
fi

echo "Updating apt.."
apt-get update
apt-get install python-dev python3-dev libssl-dev libffi-dev

echo "Installing twisted and deps.."
if [[ $(which pip2 &> /dev/null; echo $?) == 0 ]]; then
	pip=$(which pip2)
else
	pip=$(which pip)
fi
echo "Using $pip for installation.."
$pip install -U twisted service_identity idna pyOpenSSL requests autobahn scapy
