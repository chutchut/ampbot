#!/usr/bin/python

from subprocess import Popen, PIPE
import socket

import re

import requests
from scapy.all import *

class Exec():
    def execCmd(self, cmd, sin=PIPE, sout=PIPE, serr=PIPE):
        return Popen(cmd, bufsize=-1, stdin=sin, stdout=sout, stderr=serr, shell=True)
    
    def getOutput(self, proc):
        return proc.communicate()
    
    def getOutputLines(self, proc):
        lines = []
        data = self.getOutput(proc)
        if (data is not None):
            for line in data:
                line = line.rstrip()
                if (line is not None and len(line) > 0):
                    lines.append(line)
        return lines
    
    def getExitCode(self, proc):
        if (proc is not None):
            proc.wait()
            return proc.returncode
        else:
            return None
    
def isControllerPeer(cList, peer):
    isCont = False
    for c in cList:
        cont = c[0]
        if (cont.host == peer.host and cont.port == peer.port):
            isCont = True
    return isCont

def isNATPeer(pList, peer):
    isNAT = False
    for p in pList:
        pr = p[0]
        if (pr.host == peer.host and pr.port == peer.port and p[-1] is True):
            isNAT = True
    return isNAT
    
def isUp(host, port):
    try:
        # Attempt connection to host + port
        s = socket.create_connection((host, port), 3)
        if (s is not None):
            s.close()
            return True
        else:
            return False
    except:
        return False
    
def getPublicIP():
    ip = None
    try:
        data = requests.get('http://jsonip.com').json()
        ip = data['ip']
    except Exception as e:
        print('Couldnt get public IP: %s' % (e))
        ip = '0.0.0.0'
    return ip

def getPrivateIPs(excLocal=True):
    e = Exec()
    pIps = []
    p = e.execCmd('ifconfig')
    lines = e.getOutputLines(p)[0].split('\n')
    for l in lines:
        m = re.search('inet addr:(\d+.\d+.\d+.\d+)', l)
        if (m is not None):
            ip = m.group(1)
            if (excLocal and ip == '127.0.0.1'):
                continue
            pIps.append(ip)
    return pIps

def isDHCP(t=5):
    if (getDHCP(t) is False):
        return False
    else:
        return True
    
def isDHCPAnalytic(pub, pri):
    if (pub in pri):
        return False
    else:
        return True

def getDHCP(t=5):
    # Turn off response IP address validation
    conf.checkIPaddr = False
    # Set up the interface
    fam, hw = get_if_raw_hwaddr(conf.iface)
    dhcp_discover = Ether(dst="ff:ff:ff:ff:ff:ff")/IP(src="0.0.0.0",dst="255.255.255.255")/UDP(sport=68,dport=67)/BOOTP(chaddr=hw)/DHCP(options=[("message-type","discover"),"end"])
    ans, unans = srp(dhcp_discover, multi=True, timeout=t)
    if (len(ans) < 1):
        print('No DHCP responses received within %d seconds, assuming static address..' % (t))
        return False
    else:
        print('Got a DHCP response within %d seconds. This IP is dynamic' % (t))
        return ans
    