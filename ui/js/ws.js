var sock = null;
var serverSwitch = false;

function initWs() {
	var wsuri;
	var wsproto;

	if (window.location.protocol === "https:") {
		wsproto = "wss";
	} else {
		wsproto = "ws";
	}
	
	if (window.location.protocol === "file:") {
		wsuri = wsproto + "://127.0.0.1:8080/ws";
	} else {
		wsuri = wsproto + "://" + window.location.hostname + ":8080/ws";
	}
	
	if ("WebSocket" in window) {
		sock = new WebSocket(wsuri);
	} else if ("MozWebSocket" in window) {
		sock = new MozWebSocket(wsuri);
	} else {
		log("Browser does not support WebSocket!");
		window.location = "http://autobahn.ws/unsupportedbrowser";
	}
	
	if (sock) {
		sock.onopen = function() {
			log("Connected to " + wsuri);
		}
		sock.onclose = function(e) {
			log("Connection closed (wasClean = " + e.wasClean + ", code = " + e.code + ", reason = '" + e.reason + "')");
			sock = null;
			$("#peer-num").text("");
			$("#peer-list").empty();
			$("#action-menu").hide();
			$("#s-host").text("Disconnected from server");
		}
		sock.onmessage = function(e) {
			log("Got: " + e.data);
			respHandler(e);
		}
		sock.onerror = function(e) {
			log("ERR: " + e.data);
		}
	}
	
	return sock;
}

function log(m) {
	console.log(m);
}

function convertTimestamp(ts) {
	var date = new Date();
	date.setTime(ts*1000);
	return date.toString();
}

function respHandler(obj) {
	var data = JSON.parse(obj.data);
	var action = data["action"];
	switch (action) {
		case "set-server":
			$("#s-host").text("Current server is: " + data["data"] + ". Set at " + convertTimestamp(data["u"]))
			break;
		case "set-peers":
			peers = data["data"];
			if (serverSwitch == false) {
				$("#peer-list").empty();
				if (peers.length > 0) {
					$("#action-menu").show();
					$("#peer-select").empty();
					$("#peer-select").append('<option value="0">All</option>');
					$("#peer-num").html("<strong>" + peers.length + " peers connected.</strong> Updated at " + convertTimestamp(data["u"]));
					for (var i = 0; i < peers.length; i++) {
						p = peers[i];
						$("#peer-list").append('<div class="peer"><p>Host: ' + p[0] + ', iport: ' + p[3] + ', connection time: ' + convertTimestamp(p[2]) + ', uname: ' + p[4] + '</p></div>');
						$("#peer-select").append('<option value="' + p[3] + '">' + p[0] + ':' + p[3] + '</option>');
					}
				} else {
					$("#peer-num").text("");
					$("#action-menu").hide();
					$("#peer-list").append("<p>No peers connected (at " + convertTimestamp(data["u"]) + ")</p>");
				}
			}
			break;
		case "server-switch-start":
			$("#s-host").text("Server switch in progress.. Started at " + convertTimestamp(data["u"]));
			serverSwitch = true;
			break;
		case "server-switch-finish":
			$("#s-host").text("Server switch completed. New server is: " + data["data"] + ". Finished at " + convertTimestamp(data["u"]));
			serverSwitch = false;
			break;
		case "cmd-result":
			$("#cmd-output").text(data["data"]);
			break;
		default:
			log("Unknown action: " + action);
			break;
	}
}